//
//  Mt1.cpp
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#include "Mt1.h"

MachineTool1::MachineTool1() : MachineTool()
{
  name = "PAMA";
  
  range[0] = 500;
  range[1] = 400;
  range[2] = 300;

  head_dim[0] = 75;
  head_dim[1] = 100;
  head_dim[2] = 200;

  table_dim[0] = 500;
  table_dim[1] = 40;
  table_dim[2] = 300;

  x_dim[0] = 750;
  x_dim[1] = 40;
  x_dim[2] = 150;

  y_dim[0] = 100;
  y_dim[1] = 500;
  y_dim[2] = 130;

  z_dim[0] = 50;
  z_dim[1] = 50;
  z_dim[2] = 400;

}

void MachineTool1::translate_axes()
{
  
}

void MachineTool1::plot_point(point_t point, GLfloat size)
{
  glPushMatrix();
  glColor4f(1,1,1,1);
  glTranslatef(point.x, point.y, point.z);
  glutSolidSphere(size, 8, 8);
  glPopMatrix();
}


void MachineTool1::display()
{
  if (coord.x > range[0] / 2.0) {
    coord.x = range[0] / 2.0;
  }
  if (coord.x < -range[0] / 2.0) {
    coord.x = -range[0] / 2.0;
  }
  if (coord.y > range[1] ) {
    coord.y = range[1];
  }
  if (coord.y < 0.0) {
    coord.y = 0.0;
  }
  if (coord.z + tool_length > range[2] / 2.0) {
    coord.z = range[2] / 2.0 - tool_length;
  }
  if (coord.z < -range[2] / 2.0) {
    coord.z = -range[2] / 2.0 ;
  }

  if (draw_box) {
    glPushMatrix();
    glLineWidth(1.0);
    this->place(body_volume);
    glColor4f(1, 1, 1, 0.5);
    glutWireCube(1.0);
    glPopMatrix();
    glLineWidth(1.0);
  }

  /* Table */
  glLineWidth(1.0);
  glPushMatrix();
  this->place(body_table);
  glColor4f(0.5,0.5,0.5,0.9);
  glutSolidCube(1);
  glColor4f(0.4,0.4,0.4,1);
  glutWireCube(1.0);
  glPopMatrix();

  // RED Body
  glLineWidth(1.0);
  glPushMatrix();
  this->place(body_X);
  glColor4f(1,0,0,0.9);
  glutSolidCube(1);
  glColor4f(0.7,0,0,1);
  glutWireCube(1.0);
  glPopMatrix();

  // GREEN Body
  glPushMatrix();
  body_Y->center[0] = coord.x;
  this->place(body_Y);
  glColor4f(0,1,0,0.9);
  glutSolidCube(1.0);
  glColor4f(0,0.7,0,1);
  glutWireCube(1.0);
  glPopMatrix();

  // DARK BLUE Body
  glPushMatrix();
  body_head->center[0] = coord.x;
  body_head->center[1] = coord.y;
  this->place(body_head);
  glColor4f(0,0,0.7, 0.9);
  glutSolidCube(1.0);
  glColor4f(0,0,0.5,1);
  glutWireCube(1.0);
  glPopMatrix();

  // BLUE Body
  glPushMatrix();
  body_Z->center[0] = coord.x;
  body_Z->center[1] = coord.y;
  body_Z->center[2] = coord.z;
  this->place(body_Z);
  glColor4f(0,0,1, 0.9);
  glutSolidCube(1.0);
  glColor4f(0,0,0.7,1);
  glutWireCube(1.0);
  glPopMatrix();

  /* Tool */
  GLUquadric *quad;
  quad = gluNewQuadric();
  glPushMatrix();
  glColor4f(1,0.5,0,1);
  glTranslatef(coord.x, coord.y, coord.z+(tool_ballpoint ? tool_radius : 0.0));
  gluCylinder(quad, tool_radius, tool_radius, tool_length, 20, 1);
  glScalef(1.0, 1.0, (tool_ballpoint ? 1.0 : 0.0));
  gluSphere(quad, tool_radius, 20, 20);
  glPopMatrix();

  if (draw_axes) {
    glPushMatrix();
    translate_axes();
    glLineWidth(3.0);
    glColor4f(1, 0, 0, 0.66);
    glBegin(GL_LINES);
    glVertex3f(0,0,0);
    glVertex3f(axes_length,0,0);
    glEnd();
    glPushMatrix();
    glTranslatef(axes_length,0,0);
    glRotatef(90,0,1,0);
    glutSolidCone(axes_length/10.0, axes_length/5.0, 20, 1);
    glPopMatrix();

    glColor4f(0,1,0,0.66);
    glBegin(GL_LINES);
    glVertex3f(0,0,0);
    glVertex3f(0,axes_length,0);
    glEnd();
    glPushMatrix();
    glTranslatef(0,axes_length,0);
    glRotatef(-90,1,0,0);
    glutSolidCone(axes_length/10.0, axes_length/5.0, 20, 1);
    glPopMatrix();

    glColor4f(0,0,1,0.66);
    glBegin(GL_LINES);
    glVertex3f(0,0,0);
    glVertex3f(0,0,axes_length);
    glEnd();
    glPushMatrix();
    glTranslatef(0,0,axes_length);
    glRotatef(0,0,1,0);
    glutSolidCone(axes_length/10.0, axes_length/5.0, 20, 1);
    glLineWidth(1.0);
    glPopMatrix();
    glPopMatrix();
  }



  glLineWidth(1.0);

}


void MachineTool1::init_objects()
{
  /* Working volume */
  body_volume->center[0] = 0;
  body_volume->center[1] = 0;
  body_volume->center[2] = 0;
  body_volume->dims[0] = range[0];
  body_volume->dims[1] = range[1];
  body_volume->dims[2] = range[2] - tool_length;
  body_volume->handle[0] = 0;
  body_volume->handle[1] = -range[1]/2.0;
  body_volume->handle[2] = tool_length/2.0;

  /* X axis */
  body_X->center[0] = 0;
  body_X->center[1] = -head_dim[1]/2.0;
  body_X->center[2] = range[2]/2.0;
  body_X->dims[0] = x_dim[0];
  body_X->dims[1] = x_dim[1];
  body_X->dims[2] = x_dim[2];
  body_X->handle[0] = 0;
  body_X->handle[1] = x_dim[1]/2.0;
  body_X->handle[2] = -x_dim[2]/2.0;

  /* Y axis */
  body_Y->center[0] = 0;
  body_Y->center[1] = 0;
  body_Y->center[2] = range[2]/2.0 + x_dim[2];
  body_Y->dims[0] = y_dim[0];
  body_Y->dims[1] = y_dim[1];
  body_Y->dims[2] = y_dim[2];
  body_Y->handle[0] = y_dim[0]/2.0 + z_dim[0]/2.0;
  body_Y->handle[1] = -y_dim[1]/2.0 + head_dim[1]/2.0;
  body_Y->handle[2] = y_dim[2]/2.0;

  /* Z axis */
  body_Z->center[0] = 0;
  body_Z->center[1] = 0;
  body_Z->center[2] = 0;
  body_Z->dims[0] = z_dim[0];
  body_Z->dims[1] = z_dim[1];
  body_Z->dims[2] = z_dim[2];
  body_Z->handle[0] = 0;
  body_Z->handle[1] = 0;
  body_Z->handle[2] = -z_dim[2]/2.0-tool_length;

  /* Z head */
  body_head->center[0] = 0;
  body_head->center[1] = 0;
  body_head->center[2] = range[2]/2.0 + (x_dim[2]-y_dim[2]);
  body_head->dims[0] = head_dim[0];
  body_head->dims[1] = head_dim[1];
  body_head->dims[2] = head_dim[2];
  body_head->handle[0] = -head_dim[0]/2.0 + z_dim[0]/2.0;
  body_head->handle[1] = 0.0;
  body_head->handle[2] = -head_dim[2]/2.0;

  /* Table */
  body_table->center[0] = 0;
  body_table->center[1] = -head_dim[1]/2.0;
  body_table->center[2] = 0.0;
  body_table->dims[0] = table_dim[0];
  body_table->dims[1] = table_dim[1];
  body_table->dims[2] = table_dim[2];
  body_table->handle[0] = 0.0;
  body_table->handle[1] = x_dim[1]/2.0;
  body_table->handle[2] = 0.0;
}

