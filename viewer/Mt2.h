//
//  Mt2.h
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#ifndef __viewer__Mt2__
#define __viewer__Mt2__

#include "viewer.hh"

class MachineTool2 : public MachineTool {
public:
  void display();
  void init_objects();
  void describe();
  void translate_axes();
  void plot_point(point_t point, GLfloat size);
  
  MachineTool2();
  
};

#endif /* defined(__viewer__Mt2__) */
